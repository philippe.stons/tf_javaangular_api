package com.example.tfjavaapi.services;

import java.util.List;

import com.example.tfjavaapi.models.dtos.UserDTO;
import com.example.tfjavaapi.models.forms.UserInsertForm;
import com.example.tfjavaapi.models.forms.UserUpdateForm;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    List<UserDTO> getAll();
    UserDTO getOneById(Long id);
    UserDTO insert(UserInsertForm form);
    Long delete(Long id);
    UserDTO update(UserUpdateForm form, Long id);
}
