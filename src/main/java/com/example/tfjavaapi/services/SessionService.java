package com.example.tfjavaapi.services;

import com.example.tfjavaapi.models.dtos.UserDTO;
import com.example.tfjavaapi.models.forms.UserLoginForm;
import com.example.tfjavaapi.models.forms.UserRegisterForm;

public interface SessionService {
    UserDTO login(UserLoginForm form);
    UserDTO register(UserRegisterForm form);
}
