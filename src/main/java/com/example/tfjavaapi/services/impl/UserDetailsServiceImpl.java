package com.example.tfjavaapi.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.example.tfjavaapi.mappers.UserMapper;
import com.example.tfjavaapi.models.dtos.UserDTO;
import com.example.tfjavaapi.models.entities.User;
import com.example.tfjavaapi.models.forms.UserInsertForm;
import com.example.tfjavaapi.models.forms.UserUpdateForm;
import com.example.tfjavaapi.repositories.UserRepository;
import com.example.tfjavaapi.services.UserService;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserService {
    private final PasswordEncoder encoder;
    private final UserRepository repository;
    private final UserMapper mapper;

    
    public UserDetailsServiceImpl(PasswordEncoder encoder, UserRepository repository, UserMapper mapper) 
    {
        this.encoder = encoder;
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public List<UserDTO> getAll() {
        return repository.findAll()
            .stream()
            .map(mapper::entityToDto)
            .collect(Collectors.toList());
    }

    @Override
    public UserDTO getOneById(Long id) {
        return repository.findById(id)
            .map(mapper::entityToDto)
            .orElseThrow(() -> new IllegalArgumentException("User does not exist!"));
    }

    @Override
    public UserDTO insert(UserInsertForm form) {
        User user = mapper.formToEntity(form);

        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);

        user.setPassword(encoder.encode(form.getPassword()));

        return mapper.entityToDto(repository.save(user));
    }

    @Override
    public Long delete(Long id) {
        User user = repository.findById(id)
            .orElseThrow(() -> new IllegalArgumentException("User with ID : " + id + " does not exist!"));

        repository.delete(user);
        return user.getId();
    }

    @Override
    public UserDTO update(UserUpdateForm form, Long id) {
        User user = repository.findById(id)
            .orElseThrow(() -> new IllegalArgumentException("User with ID : " + id + " does not exist!"));
        
        user.setPassword(encoder.encode(form.getPassword()));
        user.setRoles(form.getRoles());

        return mapper.entityToDto(repository.save(user));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findByUsername(username)
            .orElseThrow(() -> new IllegalArgumentException("User with username : " + username + " does not exist!"));
    }  
}
