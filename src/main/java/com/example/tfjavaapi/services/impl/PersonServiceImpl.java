package com.example.tfjavaapi.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.example.tfjavaapi.mappers.PersonMapper;
import com.example.tfjavaapi.models.dtos.PersonDTO;
import com.example.tfjavaapi.models.entities.Person;
import com.example.tfjavaapi.models.forms.PersonForm;
import com.example.tfjavaapi.repositories.PersonRepository;
import com.example.tfjavaapi.services.BaseService;

import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements BaseService<PersonDTO, PersonForm, Long> {
    private final PersonRepository personRepository;
    private final PersonMapper personMapper;

    public PersonServiceImpl(PersonRepository personRepository, PersonMapper personMapper) {
        this.personRepository = personRepository;
        this.personMapper = personMapper;
    }

    @Override
    public List<PersonDTO> getAll() {
        return this.personRepository.findAll()
            .stream()
            .map(this.personMapper::entityToDto)
            .collect(Collectors.toList());
    }

    @Override
    public PersonDTO getOneById(Long id) {
        return this.personMapper.entityToDto(this.personRepository.findById(id).orElse(null));
    }

    @Override
    public PersonDTO insert(PersonForm form) {
        Person p = this.personMapper.formToEntity(form);
        return this.personMapper.entityToDto(this.personRepository.save(p));
    }

    @Override
    public Long delete(Long id) {
        Person p = this.personRepository.findById(id).orElse(null);

        if(p != null)
        {
            this.personRepository.delete(p);
            return p.getPersonId();
        }

        return -1L;
    }

    @Override
    public PersonDTO update(PersonForm form, Long id) {
        Person p = this.personRepository.findById(id).orElse(null);

        if(p != null)
        {
            p.setFirstname(form.getFirstname());
            p.setLastname(form.getLastname());
            this.personRepository.save(p);
        }

        return this.personMapper.entityToDto(p);
    }
    
}
