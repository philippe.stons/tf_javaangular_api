package com.example.tfjavaapi.services.impl;

import java.util.List;

import com.example.tfjavaapi.configs.jwt.JwtTokenProvider;
import com.example.tfjavaapi.mappers.UserMapper;
import com.example.tfjavaapi.models.dtos.UserDTO;
import com.example.tfjavaapi.models.entities.User;
import com.example.tfjavaapi.models.forms.UserLoginForm;
import com.example.tfjavaapi.models.forms.UserRegisterForm;
import com.example.tfjavaapi.repositories.UserRepository;
import com.example.tfjavaapi.services.SessionService;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SessionServiceImpl implements SessionService {
    private final PasswordEncoder encoder;
    private final UserRepository repository;
    private final UserMapper mapper;
    private final JwtTokenProvider provider;
    private AuthenticationManager manager;

    public SessionServiceImpl(PasswordEncoder encoder, UserRepository repository, UserMapper mapper,
            JwtTokenProvider provider, AuthenticationManager manager) {
        this.encoder = encoder;
        this.repository = repository;
        this.mapper = mapper;
        this.provider = provider;
        this.manager = manager;
    }

    @Override
    public UserDTO login(UserLoginForm form) {
        try
        {
            User u = repository.findByUsername(form.getUsername())
                .orElseThrow(() -> new IllegalArgumentException("User does not exist!"));

            manager.authenticate(new UsernamePasswordAuthenticationToken(form.getUsername(), form.getPassword()));

            UserDTO dto = mapper.entityToDto(u);

            dto.setToken(provider.createToken(u));

            return dto;
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("User does not exist!");
        }
    }

    @Override
    public UserDTO register(UserRegisterForm form) {
        User u = mapper.formToEntity(form);
        u.setRoles(List.of("USER"));
        u.setPassword(encoder.encode(form.getPassword()));

        u.setAccountNonExpired(true);
        u.setAccountNonLocked(true);
        u.setCredentialsNonExpired(true);
        u.setEnabled(true);

        UserDTO dto = mapper.entityToDto(repository.save(u));
        dto.setToken(provider.createToken(u));

        return dto;
    }
    
}
