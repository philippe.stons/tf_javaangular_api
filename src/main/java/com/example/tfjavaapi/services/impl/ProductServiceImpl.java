package com.example.tfjavaapi.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.example.tfjavaapi.mappers.ProductMapper;
import com.example.tfjavaapi.models.dtos.ProductDTO;
import com.example.tfjavaapi.models.entities.Product;
import com.example.tfjavaapi.models.forms.ProductForm;
import com.example.tfjavaapi.repositories.ProductRepository;
import com.example.tfjavaapi.services.BaseService;

import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements BaseService<ProductDTO, ProductForm, Long>{
    
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Override
    public List<ProductDTO> getAll() {
        return this.productRepository.findAll()
            .stream()
            .map(this.productMapper::entityToDto)
            .collect(Collectors.toList());
    }

    @Override
    public ProductDTO getOneById(Long id) {
        return this.productMapper.entityToDto(this.productRepository.findById(id).orElse(null));
    }

    @Override
    public ProductDTO insert(ProductForm form) {
        Product p = this.productMapper.formToEntity(form);
        return this.productMapper.entityToDto(this.productRepository.save(p));
    }

    @Override
    public Long delete(Long id) {
        Product p = this.productRepository.findById(id).orElse(null);
        this.productRepository.delete(p);
        return p.getProductId();
    }

    @Override
    public ProductDTO update(ProductForm form, Long id) {
        Product p = this.productRepository.findById(id).orElse(null);

        p.setCategory(form.getCategory());
        p.setName(form.getName());
        p.setPrice(form.getPrice());
        this.productRepository.save(p);

        return this.productMapper.entityToDto(p);
    }
}
