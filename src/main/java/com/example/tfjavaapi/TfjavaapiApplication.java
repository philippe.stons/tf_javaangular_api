package com.example.tfjavaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TfjavaapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TfjavaapiApplication.class, args);
	}

}
