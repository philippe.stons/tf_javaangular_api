package com.example.tfjavaapi.controllers;

import com.example.tfjavaapi.models.dtos.UserDTO;
import com.example.tfjavaapi.models.forms.UserLoginForm;
import com.example.tfjavaapi.models.forms.UserRegisterForm;
import com.example.tfjavaapi.services.SessionService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SessionController {
    private final SessionService service;

    public SessionController(SessionService service) {
        this.service = service;
    }

    @PostMapping("/login")
    public ResponseEntity<UserDTO> login(@RequestBody UserLoginForm form)
    {
        return ResponseEntity.ok(service.login(form));
    }

    @PostMapping("/register")
    public ResponseEntity<UserDTO> register(@RequestBody UserRegisterForm form)
    {
        return ResponseEntity.ok(service.register(form));
    }
}
