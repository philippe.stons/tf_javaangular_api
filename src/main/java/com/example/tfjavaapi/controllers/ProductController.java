package com.example.tfjavaapi.controllers;

import java.util.List;

import javax.validation.Valid;

import com.example.tfjavaapi.models.dtos.ProductDTO;
import com.example.tfjavaapi.models.forms.ProductForm;
import com.example.tfjavaapi.services.impl.ProductServiceImpl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/product")
public class ProductController {
    private final ProductServiceImpl productServiceImpl;

    public ProductController(ProductServiceImpl productServiceImpl) {
        this.productServiceImpl = productServiceImpl;
    }

    @GetMapping
    public ResponseEntity<List<ProductDTO>> getList()
    {
        return ResponseEntity.ok(this.productServiceImpl.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> getOneById(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.productServiceImpl.getOneById(id));
    }

    @PostMapping()
    public ResponseEntity<ProductDTO> insertOne(@Valid @RequestBody ProductForm form)
    {
        return ResponseEntity.ok(this.productServiceImpl.insert(form));
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<ProductDTO> updateOne(@PathVariable(name = "id") Long id,
        @Valid @RequestBody ProductForm form)
    {
        return ResponseEntity.ok(this.productServiceImpl.update(form, id));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Long> deleteOne(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.productServiceImpl.delete(id));
    }
}
