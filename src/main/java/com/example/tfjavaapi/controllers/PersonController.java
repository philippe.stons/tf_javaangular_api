package com.example.tfjavaapi.controllers;

import java.util.List;

import javax.validation.Valid;

import com.example.tfjavaapi.models.dtos.PersonDTO;
import com.example.tfjavaapi.models.forms.PersonForm;
import com.example.tfjavaapi.services.impl.PersonServiceImpl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/person")
public class PersonController {
    private final PersonServiceImpl personService;

    public PersonController(PersonServiceImpl personService) {
        this.personService = personService;
    }

    @GetMapping
    public ResponseEntity<List<PersonDTO>> getList()
    {
        return ResponseEntity.ok(this.personService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonDTO> getOneById(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.personService.getOneById(id));
    }

    @PostMapping()
    public ResponseEntity<PersonDTO> insertOne(@Valid @RequestBody PersonForm form)
    {
        return ResponseEntity.ok(this.personService.insert(form));
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<PersonDTO> updateOne(@PathVariable(name = "id") Long id,
        @Valid @RequestBody PersonForm form)
    {
        return ResponseEntity.ok(this.personService.update(form, id));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Long> deleteOne(@PathVariable(name = "id") Long id)
    {
        return ResponseEntity.ok(this.personService.delete(id));
    }
}
