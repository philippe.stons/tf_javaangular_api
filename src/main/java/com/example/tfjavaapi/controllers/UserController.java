package com.example.tfjavaapi.controllers;

import java.util.List;

import javax.validation.Valid;

import com.example.tfjavaapi.models.dtos.UserDTO;
import com.example.tfjavaapi.models.forms.UserInsertForm;
import com.example.tfjavaapi.models.forms.UserUpdateForm;
import com.example.tfjavaapi.services.UserService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService service;

    public UserController(UserService service)
    {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<UserDTO>> getAll()
    {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getOneByID(@PathVariable Long id)
    {
        return ResponseEntity.ok(service.getOneById(id));
    }

    @PostMapping
    public ResponseEntity<UserDTO> insert(@Valid @RequestBody UserInsertForm form)
    {
        return ResponseEntity.ok(service.insert(form));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDTO> update(@PathVariable Long id, @Valid @RequestBody UserUpdateForm form)
    {
        return ResponseEntity.ok(service.update(form, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable Long id)
    {
        return ResponseEntity.ok(service.delete(id));
    }
}
