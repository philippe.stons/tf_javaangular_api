package com.example.tfjavaapi.models.forms;

import java.util.List;

import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

@Data
@Validated
public class UserUpdateForm {
    @Length(min = 8)
    private String password;
    private List<String> roles;
}
