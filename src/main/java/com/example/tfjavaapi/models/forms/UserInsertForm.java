package com.example.tfjavaapi.models.forms;

import java.util.List;

import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

@Data
@Validated
public class UserInsertForm {
    @Length(min = 4, max = 20)
    private String username;

    // Habituellement au moins > 8
    @Length(min = 5)
    private String password;

    private List<String> roles;
}
