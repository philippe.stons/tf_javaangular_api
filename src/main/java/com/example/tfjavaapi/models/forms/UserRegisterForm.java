package com.example.tfjavaapi.models.forms;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRegisterForm {
    @Length(min = 4, max = 20)
    private String username;
    // Habituellement au moins > 8
    @Length(min = 5)
    private String password;

}
