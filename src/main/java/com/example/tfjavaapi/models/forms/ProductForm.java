package com.example.tfjavaapi.models.forms;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ProductForm {
    @NotNull
    @NotBlank
    String category;

    @NotNull
    @NotBlank
    String name;

    @Min(0)
    double price;
}
