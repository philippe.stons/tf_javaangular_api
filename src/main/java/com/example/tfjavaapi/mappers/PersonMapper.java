package com.example.tfjavaapi.mappers;

import com.example.tfjavaapi.models.dtos.PersonDTO;
import com.example.tfjavaapi.models.entities.Person;
import com.example.tfjavaapi.models.forms.PersonForm;

import org.springframework.stereotype.Service;

@Service
public class PersonMapper implements BaseMapper<PersonDTO, PersonForm, Person> {

    @Override
    public Person formToEntity(PersonForm form) {
        Person p = new Person();
        
        p.setFirstname(form.getFirstname());
        p.setLastname(form.getLastname());

        return p;
    }

    @Override
    public PersonDTO entityToDto(Person entity) {

        if(entity != null && entity.getPersonId() > 0)
        {
            return PersonDTO.builder()
                .id(entity.getPersonId())
                .firstname(entity.getFirstname())
                .lastname(entity.getLastname())
                .build();
        }

        return null;
    }

    @Override
    public Person dtoToEntity(PersonDTO dto) {
        Person p = new Person();

        if(dto != null && dto.getId() > 0)
        {
            p.setPersonId(dto.getId());
            p.setFirstname(dto.getFirstname());
            p.setLastname(dto.getLastname());
        }

        return p;
    }
    
}
