package com.example.tfjavaapi.mappers;

import com.example.tfjavaapi.models.dtos.UserDTO;
import com.example.tfjavaapi.models.entities.User;
import com.example.tfjavaapi.models.forms.UserInsertForm;
import com.example.tfjavaapi.models.forms.UserRegisterForm;
import com.example.tfjavaapi.models.forms.UserUpdateForm;

import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public User formToEntity(UserInsertForm form)
    {
        if(form != null)
        {
            User user = new User();

            user.setUsername(form.getUsername());
            user.setPassword(form.getPassword());
            user.setRoles(form.getRoles());

            return user;
        }
        return null;
    }

    public User formToEntity(UserUpdateForm form)
    {
        if(form != null)
        {
            User user = new User();

            user.setPassword(form.getPassword());
            user.setRoles(form.getRoles());

            return user;
        }
        return null;
    }

    public User formToEntity(UserRegisterForm form)
    {
        if(form != null)
        {
            User user = new User();

            user.setUsername(form.getUsername());
            user.setPassword(form.getPassword());

            return user;
        }
        return null;
    }

    public UserDTO entityToDto(User entity)
    {
        if(entity != null)
        {
            return UserDTO.builder()
                .id(entity.getId())
                .username(entity.getUsername())
                .roles(entity.getRoles())
                .accountNonExpired(entity.isAccountNonExpired())
                .accountNonLocked(entity.isAccountNonLocked())
                .credentialsNonExpired(entity.isCredentialsNonExpired())
                .enabled(entity.isEnabled())
                .build();
        }

        return null;
    }

    public User dtoToEntity(UserDTO dto)
    {
        if(dto != null)
        {
            User user = new User();

            user.setId(dto.getId());
            user.setUsername(dto.getUsername());
            user.setRoles(dto.getRoles());
            user.setAccountNonExpired(dto.isAccountNonExpired());
            user.setAccountNonLocked(dto.isAccountNonLocked());
            user.setCredentialsNonExpired(dto.isCredentialsNonExpired());
            user.setEnabled(dto.isEnabled());

            return user;
        }
        return null;
    }
}
