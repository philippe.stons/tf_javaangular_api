package com.example.tfjavaapi.init;

import java.util.Arrays;
import java.util.List;

import com.example.tfjavaapi.models.entities.Person;
import com.example.tfjavaapi.models.entities.User;
import com.example.tfjavaapi.repositories.PersonRepository;
import com.example.tfjavaapi.repositories.UserRepository;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DatabaseInit implements InitializingBean {
    private final PersonRepository personRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public DatabaseInit(PersonRepository personRepository, UserRepository userRepository,
            PasswordEncoder encoder) {
        this.personRepository = personRepository;
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info(">>> !! INITIALIZE DATABASE !! <<<");

        List<Person> persons = Arrays.asList(
            Person.builder()
                .firstname("Johann Sebastian")
                .lastname("Bach")
                .build(),
            Person.builder()
                .firstname("Riri")
                .lastname("Duck")
                .build(),
            Person.builder()
                .firstname("Fifi")
                .lastname("Duck")
                .build(),
            Person.builder()
                .firstname("Loulou")
                .lastname("Duck")
                .build()
        );

        persons.forEach(this.personRepository::save);

        List<User> users = Arrays.asList(
            User.builder()
                .username("admin")
                .password(this.encoder.encode("admin"))
                .roles(List.of("ADMIN", "USER"))
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build(),
            User.builder()
                .username("user")
                .password(this.encoder.encode("user"))
                .roles(List.of("USER"))
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build(),
            User.builder()
                .username("test")
                .password(this.encoder.encode("test"))
                .roles(List.of("USER"))
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build()
        );

        users.forEach(this.userRepository::save);
    }
}
